#!/usr/bin/env bash

####
#
# variables
#
####

export DEBIAN_FRONTEND=noninteractive

source /vagrant/provisionning/common/log.bash

####
#
# Début exécution
#
####

logAndPrint "Debut provisionning..."

echo "logfile : $LogFile"

logAndPrint "###"
logAndPrint "###"
logAndPrint "###"
logAndPrint "01. on fait le ménage et on met à  jour notre systeme."

yum clean all    2>&1 | tee -a $LogFile
yum check-update 2>&1 | tee -a $LogFile
yum upgrade -y   2>&1 | tee -a $LogFile

logAndPrint "###"
logAndPrint "###"
logAndPrint "###"
logAndPrint "02. on install PostgreSQL."

logAndPrint "###"
logAndPrint "02.01 On installe le depot."
rpm -Uvh http://yum.postgresql.org/9.3/redhat/rhel-6-x86_64/pgdg-centos93-9.3-1.noarch.rpm  2>&1 | tee -a $LogFile

logAndPrint "###"
logAndPrint "02.02 On installe le paquet."
yum check-update 2>&1 | tee -a $LogFile
yum upgrade -y   2>&1 | tee -a $LogFile
yum install postgresql93-server postgresql93-contrib -y 2>&1 | tee -a $LogFile

logAndPrint "###"
logAndPrint "###"
logAndPrint "###"
logAndPrint "03. On initialise PostgreSQL."
service postgresql-9.3 initdb 2>&1 | tee -a $LogFile

logAndPrint "###"
logAndPrint "###"
logAndPrint "###"
logAndPrint "04. On active son demarage au boot et on le démarre."
/etc/init.d/postgresql-9.3 start 2>&1 | tee -a $LogFile
chkconfig postgresql-9.3 on      2>&1 | tee -a $LogFile

logAndPrint "###"
logAndPrint "###"
logAndPrint "###"
logAndPrint "Fin provisionning!"
