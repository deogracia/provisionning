#!/usr/bin/env bash

###########
#
#
# Ne contient que des fonctions
#
#
###########


function install_latest_mri ()
{
 sudo -H -i -u vagrant /bin/bash -c "rvm install ruby --verify-downloads 0 --disable-binary"
}

function set_mri_default ()
{
 sudo -H -i -u vagrant /bin/bash --login -c "rvm use ruby --default"
}
