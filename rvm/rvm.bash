#!/usr/bin/env bash

####
#
# variables
#
####

export DEBIAN_FRONTEND=noninteractive

source /vagrant/provisionning/common/log.bash

####
#
# Début exécution
#
####

logAndPrint "Debut provisionning RVM..."


logAndPrint "###"
logAndPrint "###"
logAndPrint "###"
logAndPrint "01.02 On installe RVM latest stable ."
sudo -H -i -u vagrant /bin/bash -c "curl -sSL https://get.rvm.io | bash -s -- --autolibs=enable --auto-dotfiles" 2>&1 | tee -a $LogFile
if [[ $? -ne 0 ]]; then
	logAndPrint "FAIL: pb avec RVM! :/"
	exit -1
fi
sudo -H -i -u vagrant /bin/bash -c "rvm autolibs enable" 2>&1 | tee -a $LogFile


logAndPrint "###"
logAndPrint "###"
logAndPrint "###"
logAndPrint "Fin provisionning RVM!"
