#!/usr/bin/env bash


_DATE=$(/bin/date +%Y-%m-%d-%H.%M.%S)
export LogFile="/vagrant/provision_$_DATE.log"


function logAndPrint()
{
 echo "$(/bin/date +%Y-%m-%d-%T) : $1" 2>&1 | tee -a $LogFile
}

function log()
{
 echo "$(/bin/date +%Y-%m-%d-%T) : $1" 2>&1 $LogFile
}
